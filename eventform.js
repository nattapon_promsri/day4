"use strict";

// function showAlert(text) {
//     console.log(text)

// }

// function listFormElement() {
//     var formElements = document.getElementsByTagName('form');
//     console.log(formElements);
// }

// function copyTextFromNameToLastName() {
//     var nameElement = document.getElementById('firstName');
//     var surnameElement = document.getElementById('lastName');
//     var firstName = nameElement.value;

//     console.log('firstname is ' + firstName);
//     console.log('now lastname is ' + surnameElement.value);
//     surnameElement.value = firstName;

// }


function clearData() {

    var firstNameElement = document.getElementById('firstName');
    var lastNameElement = document.getElementById('lastName');
    var emailElement = document.getElementById('email');
    var passwordElement = document.getElementById('password');
    var confirmPasswordElement = document.getElementById('confirmPassword');
    var radioElement1 = document.getElementById('radio1');
    var radioElement2 = document.getElementById('radio2');
    var checkElement = document.getElementById('gridCheck');


    firstNameElement.value = "";
    lastNameElement.value = "";
    emailElement.value = "";
    passwordElement.value = "";
    confirmPasswordElement.value = "";
    radioElement1.checked = false;
    radioElement2.checked = false;
    checkElement.checked = false;

}


function status2email() {

    var radioElement1 = document.getElementById('radio1').value;
    var radioElement2 = document.getElementById('radio2').value;

    var emailElement = "";

    if (document.getElementById('radio1').checked) {
        emailElement = radioElement1;
    } else
        if (document.getElementById('radio2').checked) {
            emailElement = radioElement2;
        }

    document.getElementById('email').value = emailElement;

}

function showSummary() {
    var firstNameElement = document.getElementById('firstName').value;
    var lastNameElement = document.getElementById('lastName').value;
    var emailElement = document.getElementById('email').value;
    var passwordElement = document.getElementById('password').value;
    var radioElement1 = document.getElementById('radio1').value;
    var radioElement2 = document.getElementById('radio2').value;

    var radio = "";
    if (document.getElementById('radio1').checked) {

        radio = radioElement1;
    } else
        if (document.getElementById('radio2').checked) {
            radio = radioElement2;
        }

    var summaryElemant = document.getElementById('summary');

    summaryElemant.innerHTML = firstNameElement + " " + lastNameElement + " " + emailElement + " " + passwordElement + " " + radio;
    summaryElemant.removeAttribute('hidden');

}

function showSummaryIfNoDataNotShow() {
    var firstNameElement = document.getElementById('firstName').value;
    var lastNameElement = document.getElementById('lastName').value;
    var emailElement = document.getElementById('email').value;
    var passwordElement = document.getElementById('password').value;
    var radioElement1 = document.getElementById('radio1').value;
    var radioElement2 = document.getElementById('radio2').value;

    var radio = "";
    if (document.getElementById('radio1').checked) {

        radio = radioElement1;
    } else
        if (document.getElementById('radio2').checked) {
            radio = radioElement2;
        }

    var summaryElemant = document.getElementById('summary');

    summaryElemant.innerHTML = firstNameElement + " " + lastNameElement + " " +
        emailElement + " " + passwordElement + " " + radio;

    if (firstNameElement != '') {
        summaryElemant.innerHTML = firstNameElement + " " + lastNameElement + " " + emailElement + " " + passwordElement + " " + radio;
        summaryElemant.removeAttribute('hidden');
    } else
        if (lastNameElement != '') {
            summaryElemant.innerHTML = firstNameElement + " " + lastNameElement + " " + emailElement + " " + passwordElement + " " + radio;
            summaryElemant.removeAttribute('hidden');
        } else
            if (emailElement != '') {
                summaryElemant.innerHTML = firstNameElement + " " + lastNameElement + " " + emailElement + " " + passwordElement + " " + radio;
                summaryElemant.removeAttribute('hidden');
            } else
                if (!document.getElementById('radio1').checked) {
                    summaryElemant.innerHTML = firstNameElement + " " + lastNameElement + " " + emailElement + " " + passwordElement + " " + radio;
                    summaryElemant.removeAttribute('hidden');
                } else
                    if (!document.getElementById('radio2').checked) {
                        summaryElemant.innerHTML = firstNameElement + " " + lastNameElement + " " + emailElement + " " + passwordElement + " " + radio;
                        summaryElemant.removeAttribute('hidden');
                    } else {
                        summaryElement.hidden = true;
                    }

}

// function addElement() {
//     var node = document.createElement('p');
//     node.innerHTML = 'Hello world';
//     var placeHolder = document.getElementById('placeholder');
//     placeHolder.appendChild(node);
// }

// function addHref() {
//     var node = document.createElement('a');
//     node.innerHTML = 'google';
//     node.setAttribute('href', 'https://wwww.google.com');
//     node.setAttribute('target', '_blank');
//     var placeHolder = document.getElementById('placeholder');
//     placeHolder.appendChild(node);
// }

// function addBoth() {
//     var node1 = document.createElement('p');
//     node1.innerHTML = 'Hello world';
//     var node2 = document.createElement('a');
//     node2.innerHTML = 'google';

//     node2.setAttribute('href', 'https://wwww.google.com');
//     node2.setAttribute('target', '_blank');

//     var placeHolder = document.getElementById('placeholder');
//     placeHolder.appendChild(node1);
//     placeHolder.appendChild(node2);
// }



function showForm() {

    var head = document.createElement('p');
    head.innerHTML = 'Please fill in this form to create account';

    var firstName = document.createElement('input');
    firstName.setAttribute('type', 'text');
    firstName.setAttribute('class', 'form-control');
    firstName.setAttribute('id', 'firstName2');
    firstName.setAttribute('placeholder', 'First name');

    var lastName = document.createElement('input');
    lastName.setAttribute('type', 'text');
    lastName.setAttribute('class', 'form-control');
    lastName.setAttribute('id', 'lastName2');
    lastName.setAttribute('placeholder', 'Last name');

    var eMail = document.createElement('input');
    eMail.setAttribute('type', 'text');
    eMail.setAttribute('class', 'form-control');
    eMail.setAttribute('id', 'email2');
    eMail.setAttribute('placeholder', 'Enter E-mail');

    var divClassCol = document.createElement('div');
    divClassCol.setAttribute('class', 'col-12');

    var form = document.createElement('form');

    var divClassFormGroup = document.createElement('div');
    divClassFormGroup.setAttribute('class', 'form-group');

    var divClassFormRow = document.createElement('div');
    divClassFormRow.setAttribute('class', 'form-row');

    var divClassCol61 = document.createElement('div');
    divClassCol61.setAttribute('class', 'col-6');

    var divClassCol62 = document.createElement('div');
    divClassCol62.setAttribute('class', 'col-6');

    var divClassCol12 = document.createElement('div');
    divClassCol12.setAttribute('class', 'col-12');

    var test = document.getElementById('test2');

    test.appendChild(divClassCol);
    divClassCol.appendChild(form);
    form.appendChild(divClassFormGroup);
    divClassFormGroup.appendChild(head);
    divClassFormGroup.appendChild(divClassFormRow);


    divClassFormRow.appendChild(divClassCol61);
    divClassFormRow.appendChild(divClassCol62);
    divClassFormRow.appendChild(divClassCol12);

    divClassCol61.appendChild(firstName);
    divClassCol62.appendChild(lastName);
    divClassCol12.appendChild(eMail);

}


function showTable() {
    var test3 = document.getElementById('test3');

    var table = document.createElement('table');
    table.setAttribute('class', 'table table-striped');

    var tHead = document.createElement('thead');

    var tBody = document.createElement('tbody');

    var tr1 = document.createElement('tr');

    var tr2 = document.createElement('tr');

    var tr3 = document.createElement('tr');

    var tr4 = document.createElement('tr');

    var th1 = document.createElement('th');
    th1.innerHTML = '#';
    th1.setAttribute('scope', 'col');

    var th2 = document.createElement('th');
    th2.innerHTML = 'First';
    th2.setAttribute('scope', 'col');

    var th3 = document.createElement('th');
    th3.innerHTML = 'Last';
    th3.setAttribute('scope', 'col');

    var th4 = document.createElement('th');
    th4.innerHTML = 'Handle';
    th4.setAttribute('scope', 'col');

    var th5 = document.createElement('th');
    th5.innerHTML = '1';
    th5.setAttribute('scope', 'col');

    var th6 = document.createElement('th');
    th6.innerHTML = '2';
    th6.setAttribute('scope', 'col');

    var th7 = document.createElement('th');
    th7.innerHTML = '3';
    th7.setAttribute('scope', 'col');

    var td1 = document.createElement('td');
    td1.innerHTML = 'Mark';

    var td2 = document.createElement('td');
    td2.innerHTML = 'Otto';

    var td3 = document.createElement('td');
    td3.innerHTML = '@mdo';

    var td4 = document.createElement('td');
    td4.innerHTML = 'Jacob';

    var td5 = document.createElement('td');
    td5.innerHTML = 'Thornton';

    var td6 = document.createElement('td');
    td6.innerHTML = '@fat';

    var td7 = document.createElement('td');
    td7.innerHTML = 'Larry';

    var td8 = document.createElement('td');
    td8.innerHTML = 'the Bird';

    var td9 = document.createElement('td');
    td9.innerHTML = '@twitter';

    test3.appendChild(table)

    table.appendChild(tHead)
    table.appendChild(tBody)

    tHead.appendChild(tr1)
    tr1.appendChild(th1)
    tr1.appendChild(th2)
    tr1.appendChild(th3)
    tr1.appendChild(th4)

    tBody.appendChild(tr2)
    tBody.appendChild(tr3)
    tBody.appendChild(tr4)

    tr2.appendChild(th5)
    tr2.appendChild(td1)
    tr2.appendChild(td2)
    tr2.appendChild(td3)

    tr3.appendChild(th6)
    tr3.appendChild(td4)
    tr3.appendChild(td5)
    tr3.appendChild(td6)

    tr4.appendChild(th7)
    tr4.appendChild(td7)
    tr4.appendChild(td8)
    tr4.appendChild(td9)

}

